export class Order {
    private readonly _amount: number = 0

    constructor(amount: number){
        this._amount = amount
    }

    get amount(): number {
        return this._amount;
    }
}
