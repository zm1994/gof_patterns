import {ICalculator} from "./interfaces";
import {Order} from "./order";

export default class StripeDecorator implements ICalculator {
    public calculator: ICalculator
    private lowestUnit = 100

    public constructor(baseCalc: ICalculator) {
        this.calculator = baseCalc
    }
    getTotal(orders: Order[]): number {
        return this.calculator.getTotal(orders) * this.lowestUnit;
    }

}
