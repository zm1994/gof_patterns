import {Order} from "./order";

export interface ICalculator {
    getTotal(orders: Order[]) : number
}
