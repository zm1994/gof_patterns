import {ICalculator} from "./interfaces";
import {Order} from "./order";

export default class Calculator implements ICalculator{
    getTotal(orders: Order[]): number {
        return orders.reduce((sum, order) => sum + order.amount, 0);
    }

}
