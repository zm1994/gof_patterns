import StripeDecorator from "./StripeDecorator";
import Calculator from "./Calculator";
import {Order} from "./order";

const baseCalc = new Calculator()
/**
 * Decorator is a Conceptual pattern that allows adding new behaviors to objects dynamically
 * by placing them inside special wrapper objects.
 */
const stripeCalc = new StripeDecorator(baseCalc)
const orders = [
    new Order(100),
    new Order(200),
]

console.log('Total amount in stripe unit', stripeCalc.getTotal(orders))
