import AbstractFile from "./abstractFile";

export default class File extends AbstractFile{
    getProperties(): string {
        return "get file properties";
    }

}
