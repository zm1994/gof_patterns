export interface IFile {
    getName(): string
    getProperties(): string
    setParentFolder(folder: IFile): void
    getParentFolder(): IFile | undefined
}
