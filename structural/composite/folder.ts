import {IFile} from "./interfaces";
import AbstractFile from "./abstractFile";

export default class Folder extends AbstractFile{
    protected childrenFiles: IFile[] = []
    constructor(name: string) {
        super(name)
    }

    public addFile(file: IFile) {
        this.childrenFiles.push(file)
        file.setParentFolder(this)
        return this
    }

    public removeFile(removalFile: IFile) {
        this.childrenFiles = this.childrenFiles.filter(file => file.getName() === removalFile.getName())
    }

    public getProperties(): string {
        return this.childrenFiles.map(file => file.getProperties()).join('\n');
    }

}
