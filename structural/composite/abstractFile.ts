import {IFile} from "./interfaces";

export default abstract class AbstractFile implements IFile {
    protected name: string = ''
    protected parent: IFile | undefined

    public constructor(name: string) {
        this.name = name
    }

    getName() {
        return this.name
    }

    getParentFolder(): IFile | undefined {
        return this.parent;
    }

    setParentFolder(folder: IFile): void {
        this.parent = folder
    }

    public abstract getProperties(): string

}
