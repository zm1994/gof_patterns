import Folder from "./folder";
import File from "./file";


/**
 * Composite is a structural design pattern that lets you compose objects into tree structures
 * and then work with these structures as if they were individual objects.
 */
const rootFolder = new Folder('root')
const file = new File('file.doc')
rootFolder.addFile(file)
const folder = new Folder('inner_folder')
rootFolder.addFile(folder)
