import {IMsgSender} from "./interfaces";

export default abstract class Message {
    protected readonly msgSender: IMsgSender
    protected message: string = ''

    public constructor(msgSender: IMsgSender) {
        this.msgSender = msgSender
    }

    public abstract send(): void

    public setMessage(msg: string) {
        this.message = msg
    }
}
