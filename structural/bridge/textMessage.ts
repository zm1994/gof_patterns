import Message from "./message";
import {IMsgSender} from "./interfaces";

export default class TextMessage extends Message{
    public constructor(msgSender: IMsgSender) {
        super(msgSender)
    }

    send(): void {
        this.msgSender.send(this.message)
    }

}
