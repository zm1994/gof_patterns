export interface IMsgSender {
    send(data: string): void
}
