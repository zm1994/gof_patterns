import TextMessage from "./textMessage";
import SmsSender from "./smsSender";
import EncryptedMessage from "./encryptedMessage";

/**
 * Bridge is a structural design pattern that lets split a large class or a set of closely related classes
 * into two separate hierarchies—abstraction and implementation—which can be developed independently of each other.
 */
const smsSender = new SmsSender()
const msg = new TextMessage(smsSender)
msg.setMessage('Test')

msg.send()


const encryptedMsg = new EncryptedMessage(smsSender)
encryptedMsg.setMessage('Test')

encryptedMsg.send()
