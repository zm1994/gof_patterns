import Message from "./message";
import {IMsgSender} from "./interfaces";

export default class EncryptedMessage extends Message{
    public constructor(msgSender: IMsgSender) {
        super(msgSender)
    }

    private encryptMsg() {
        return `Encrypted msg`
    }

    send(): void {
        const msg = this.encryptMsg()
        this.msgSender.send(msg)
    }

}
