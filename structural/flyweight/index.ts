import {Area} from "./area";

const area = new Area()

/**
 * Flyweight is a structural design pattern that lets you fit more objects into the available amount of
 * RAM by sharing common parts of state between multiple objects instead of keeping all of the data in each object.
 */
area.addPoint(0, 1, '#dddddd', 20)
    .addPoint(2, 10, '#dddddd', 20)
    .addPoint(5, 11, '#dddddd', 11)

area.redrawAll()
