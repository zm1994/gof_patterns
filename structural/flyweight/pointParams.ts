export default class PointParams {
    private readonly _color: string
    private readonly _size: number

    constructor(color: string, size: number) {
        this._color = color
        this._size = size
    }

    public get params() {
        return {
            color: this._color,
            size: this._size,
        }
    }

    set(coordX: number, coordY: number) {
        console.log(`Set point on ${coordX}:${coordY} with params ${JSON.stringify(this.params)}`)
    }
}
