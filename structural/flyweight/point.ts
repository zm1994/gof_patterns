import PointParams from "./pointParams";

export default class Point {
    private readonly coordX: number
    private readonly coordY: number
    private params: PointParams

    constructor(coordX: number, coordY: number, params: PointParams) {
        this.coordX = coordX
        this.coordY = coordY
        this.params = params
    }

    setPosition() {
        this.params.set(this.coordX, this.coordY)
    }
}
