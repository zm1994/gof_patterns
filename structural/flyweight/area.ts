import Point from "./point";
import ParamsFactory from "./paramsFactory";

export class Area {
    private points: Point[]= []

    public addPoint(coordX: number, coordY: number, color: string, size: number) {
        const paramPoint = ParamsFactory.getPointParam({ color, size })
        this.points.push(new Point(coordX, coordY, paramPoint))
        return this
    }

    public redrawAll() {
        this.points.forEach(point => point.setPosition())
    }
}
