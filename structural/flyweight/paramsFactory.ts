import PointParams from "./pointParams";

export default class ParamsFactory {
    private static pointParams: PointParams[] = []

    public static getPointParam(config: any) {
        const foundParam = this.pointParams.find(
            param => JSON.stringify(config) === JSON.stringify(param.params)
        )

        if (foundParam) {
            return foundParam
        }

        const pointParam = new PointParams(config.color, config.size)

        this.pointParams.push(pointParam)

        return pointParam
    }
}
