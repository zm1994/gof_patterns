import {IScrapper} from "./interfaces";

export default class CachedProxyScrapper implements IScrapper {
    private scrapper: IScrapper
    private cachedResults: any

    constructor(scrapper: IScrapper) {
        this.scrapper = scrapper
    }

    public parsePage(url: string): any {
        if(!this.cachedResults[url]) {
            this.cachedResults[url] = this.scrapper.parsePage(url)
        }

        return this.cachedResults[url]
    }

}
