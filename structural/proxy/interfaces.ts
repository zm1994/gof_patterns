export interface IScrapper {
    parsePage(url: string): any
}
