import Scrapper from "./scrapper";
import CachedProxyScrapper from "./cachedProxyScrapper";

/**
 * Proxy is a structural design pattern that lets you provide a substitute or placeholder for another object.
 * A proxy controls access to the original object, allowing you to perform something either before or after the request gets through to the original object.
 */

const scrapper = new Scrapper()
const cachedScrapper = new CachedProxyScrapper(scrapper)

cachedScrapper.parsePage('https://www.google.com')
cachedScrapper.parsePage('https://www.google.com')
