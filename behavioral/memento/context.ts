import Backup from "./backup";

export default class Context {
    private state: any

    constructor(state: any) {
        this.state = state
    }

    public mergeState(newData: any) {
        this.state = {...this.state, ...newData}
        return this.state
    }

    public save(): Backup {
        return new Backup(this.state)
    }

    public restore(backup: Backup) {
        this.state = backup.getState()
    }
}
