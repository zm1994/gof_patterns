export default class Backup {
    private readonly id: string
    private readonly backupStr: string

    constructor(data: any) {
        this.id = `Backup_${(~~(Math.random()*1e8)).toString(16)}`
        this.backupStr = JSON.stringify(data)
    }

    public get ID() {
        return this.id
    }

    public getState() {
        return JSON.parse(this.backupStr)
    }
}
