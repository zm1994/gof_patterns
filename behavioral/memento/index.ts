import Context from "./context";
import CareTaker from "./careTaker";

/**
 * Memento is a behavioral design pattern that allows making snapshots of an object’s state and restoring it in future.
 */
const context = new Context({ name: 'test' })
const careTaker = new CareTaker(context)
careTaker.makeBackup()
context.mergeState({ name: 'tets', age: 20 })
careTaker.undoBackup()
