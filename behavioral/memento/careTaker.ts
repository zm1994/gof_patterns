import Context from "./context";
import Backup from "./backup";

export default class CareTaker {
    private context: Context
    private backups: Backup[] = []

    constructor(context: Context) {
        this.context = context
    }

    makeBackup() {
        this.backups.push(this.context.save())
    }

    undoBackup() {
        const latestBackup = this.backups.pop()
        return latestBackup && this.context.restore(latestBackup)
    }
}
