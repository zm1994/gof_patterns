export default abstract class AbstractLogger {
    protected serialise(data: any) {
        return JSON.stringify(data)
    }

    public async logData(data: any) {
        const serialised = this.serialise(data)
        await this.writeLog(serialised)
    }

    protected abstract async writeLog(serialisedData: any): Promise<any>
}
