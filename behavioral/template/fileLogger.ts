import AbstractLogger from "./abstractLogger";

export default class FileLogger extends AbstractLogger{
    protected async writeLog(serialisedData: any): Promise<any> {
        console.log('Write data to file')
    }

}
