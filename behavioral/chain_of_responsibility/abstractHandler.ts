import {IHandler, IUser} from "./interfaces";

export default abstract class AbstractHandler implements IHandler
{
    private nextHandler: IHandler | null = null;

    public setNext(handler: IHandler): IHandler {
        this.nextHandler = handler;
        return handler;
    }

    public handle(user: IUser) {
        if (this.nextHandler) {
            return this.nextHandler.handle(user);
        }

        return null;
    }
}
