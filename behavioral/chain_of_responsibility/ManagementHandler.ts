import AbstractHandler from "./abstractHandler";
import {Department, IUser} from "./interfaces";

export default class ManagementHandler extends AbstractHandler{
    handle(user: IUser): any | null {
        if (user.department === Department.management) {
            return 'Result for management department'
        }
        return super.handle(user);
    }
}
