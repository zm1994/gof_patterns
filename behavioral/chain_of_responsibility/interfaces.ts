export enum Department {
    management = 'management',
    humanResource = 'humanResource',
}

export interface IUser {
    isAdmin: boolean,
    department: Department
}

export interface IHandler {
    setNext(handler: Handler): Handler;
    handle(user: IUser): any;
}
