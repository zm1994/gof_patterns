import AdminHandler from "./AdminHandler";
import ManagementHandler from "./ManagementHandler";
import HumanResourceDepartment from "./humanResourceDepartment";
import {Department, IUser} from "./interfaces";

/**
 * Chain of Responsibility is a behavioral design pattern that lets you pass requests along a chain of handlers.
 * Upon receiving a request, each handler decides either to process the request or to pass
 * it to the next handler in the chain.
 */
const adminHandler = new AdminHandler()
const manHandler = new ManagementHandler()
const hrHandler = new HumanResourceDepartment()

adminHandler.setNext(manHandler).setNext(hrHandler)

const user: IUser = {
    isAdmin: true,
    department: Department.humanResource
}

const result = adminHandler.handle(user)
