import AbstractHandler from "./abstractHandler";
import {Department, IUser} from "./interfaces";

export default class HumanResourceDepartment extends AbstractHandler{
    handle(user: IUser): any | null {
        if (user.department === Department.humanResource) {
            return 'Result for HR department'
        }
        return super.handle(user);
    }
}
