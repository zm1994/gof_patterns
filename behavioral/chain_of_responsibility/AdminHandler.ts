import {IHandler, IUser} from "./interfaces";
import AbstractHandler from "./abstractHandler";

export default class AdminHandler extends  AbstractHandler{
    handle(user: IUser): any | null {
        if (!user.isAdmin) {
            throw new Error('User is not admin')
        }
        return super.handle(user);
    }

}
