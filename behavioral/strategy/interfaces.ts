export interface IPaymentData {
    amount: number,
    currency: string,
    metadata: any
}

export interface IPaymentStrategy {
    makePayment(data: IPaymentData): any
}
