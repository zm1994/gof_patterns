import PaymentContext from "./paymentContext";
import StripeStrategy from "./stripeStrategy";

/**
 * Strategy is a behavioral design pattern that lets you define a family of algorithms,
 * put each of them into a separate class, and make their objects interchangeable.
 */
const paymentContext = new PaymentContext()
paymentContext.setStrategy(new StripeStrategy())
paymentContext.pay({ amount: 20, currency: 'usd', metadata: {} })
