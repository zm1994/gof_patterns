import {IPaymentData, IPaymentStrategy} from "./interfaces";

export default class PaymentContext {
    private strategy: IPaymentStrategy | undefined

    public setStrategy(strategy: IPaymentStrategy) {
        this.strategy = strategy
    }

    public pay(data: IPaymentData) {
        return this.strategy && this.strategy.makePayment(data)
    }
}
