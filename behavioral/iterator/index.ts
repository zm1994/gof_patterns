import Iterator from "./iterator";

const collection = [12, 10, 10]

const iterator = new Iterator(collection)

while (iterator.hasNext()) {
    console.log(`Item: ( ${iterator.current()} )`)
    iterator.next()
}
