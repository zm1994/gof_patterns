import {IIterator} from "./interface";

export default class Iterator<T> implements IIterator<T> {
    private readonly collection: Array<T> = []
    private currentIdx: number = 0

    constructor(collection: Array<T>) {
        this.collection = collection
        this.currentIdx = 0
    }

    current(): T {
        return this.collection[this.currentIdx]
    }

    hasNext(): boolean {
        return !!this.collection[this.currentIdx + 1];
    }

    next(): T | undefined {
        if (this.hasNext()) {
            this.currentIdx = this.currentIdx + 1
            return this.collection[this.currentIdx]
        }

        return undefined
    }

    rewind(): void {
        this.currentIdx = 0
    }
    
}
