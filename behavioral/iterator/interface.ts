export interface IIterator<T> {
    current(): any
    hasNext(): boolean
    next(): T | undefined
    rewind(): void
}
