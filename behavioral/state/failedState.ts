import {ICardState} from "./interfaces";
import AbstractState from "./abstractState";

export default class FailedState extends AbstractState{
    handle(data: any): any {
        return this.toString()
    }

    toString() {
        return 'failed'
    }
}
