import {ICardState} from "./interfaces";
import CheckingState from "./checkingState";

export default class Context {
    private state: ICardState | undefined

    public setState(state: ICardState) {
        this.state = state
    }

    public setCard(data: any) {
        this.setState(new CheckingState(this))
        return this.state && this.state.handle(data)
    }

    public getCardStatus() {
        return this.state && this.state.toString()
    }
}
