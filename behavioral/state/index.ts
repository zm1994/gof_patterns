import Context from "./context";

/**
 * State is a behavioral design pattern that lets an object alter its behavior when its internal state changes.
 * It appears as if the object changed its class.
 */
const context = new Context()
context.setCard({ cvv: '233', number: '424242424242442' })
context.getCardStatus()
