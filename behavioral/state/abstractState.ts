import {ICardState} from "./interfaces";
import Context from "./context";

export default abstract class AbstractState implements ICardState{
    protected context: Context

    public constructor(context: Context) {
        this.context = context
    }
    abstract handle(data: any): any
}
