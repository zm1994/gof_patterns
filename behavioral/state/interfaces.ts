export interface ICardState {
    handle(data: any): any
    toString(): string
}
