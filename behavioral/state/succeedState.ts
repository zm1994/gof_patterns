import AbstractState from "./abstractState";

export default class SucceedState extends AbstractState{
    handle(data: any): any {
        console.log('Final state of order')
    }

    toString() {
        return 'succeed'
    }

}
