import {ICardState} from "./interfaces";
import AbstractState from "./abstractState";
import SucceedState from "./succeedState";
import FailedState from "./failedState";

export default class CheckingState extends AbstractState{
    async handle(data: any): Promise<any> {
        console.log('Execute payment state')
        const result = await fetch('/api/payment') as any
        if (result === 'succeed') {
            this.context.setState(new SucceedState(this.context))
        } else {
            this.context.setState(new FailedState(this.context))
        }
    }

    toString() {
        return 'checking'
    }

}
