import {IObserver} from "./interfaces";

export default class Observer implements IObserver{
    public readonly id: string;

    constructor() {
        this.id = `observer_${Math.random().toString(36).substr(2, 9)}`
    }

    notify(data: any): void {
        console.log(`Data notification for observer ${this.id}: ( ${data} )`)
    }

}
