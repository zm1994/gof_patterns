export interface IObserver {
    id: string
    notify(data: any): void
}

export interface IPublisher {
    attach(observer: IObserver): void
    detach(observer: IObserver): void
    notifyAll(data: any): void
}
