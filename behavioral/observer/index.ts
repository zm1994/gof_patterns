import Observer from "./observer";
import DataPublisher from "./dataPublisher";

const observer = new Observer()

const publisher = new DataPublisher()

publisher.attach(observer)

publisher.notifyAll('example data notification')
