import {IObserver, IPublisher} from "./interfaces";

export default class DataPublisher implements IPublisher{
    private observers: IObserver[] = []
    attach(observer: IObserver): void {
        this.observers.push(observer)
    }

    detach(delObserver: IObserver): void {
        this.observers = this.observers.filter(observer => observer.id === delObserver.id)
    }

    notifyAll(data: any): void {
        this.observers.forEach(observer => observer.notify(data))
    }
}
