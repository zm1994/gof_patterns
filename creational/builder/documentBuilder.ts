interface ITextOptions {
    font: string
    color: string
    size: string
}

export default class DocumentBuilder {
    private header: string = ''
    private body: string = ''
    private footer: string = ''
    public setHeader(text: string, opts: ITextOptions) {
        console.log('Header with', opts)
        this.header = text
        return this
    }

    public addBodyLine(text: string, opts: ITextOptions) {
        console.log('Body line', opts)
        this.body = this.body + text
        return this
    }

    public setFooter(text: string, opts: ITextOptions) {
        console.log('Footer with', opts)
        this.footer = text
        return this
    }

    public toString() {
        return `${this.header}\n${this.body}\n${this.footer}`
    }
}
