import DocumentBuilder from "./documentBuilder";

/**
 * @description
 * Builder is a creational design pattern that lets you construct complex objects step by step.
 * The pattern allows you to produce different types and representations of an object using the same
 * construction code.
 */
const textBuilder = new DocumentBuilder()

const opt = { color: 'black', size: '12', font: 'Arial' }

const document = textBuilder
    .setHeader('Example H', opt)
    .addBodyLine('Body Line 1', opt)
    .addBodyLine('Body Line 2', opt)
    .setFooter('Footer', opt)

console.log('Doc', document)
