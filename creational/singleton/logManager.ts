export default class LogManager {
    private readonly logger: Logger
    private constructor() {}

    public static getLogger() {
        if (!this.logger) {
            this.logger = new Logger()
        }

        return this.logger
    }
}
