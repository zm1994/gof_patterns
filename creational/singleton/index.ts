import LogManager from "./logManager";

/**
 * Singleton is a creational design pattern, which ensures that only one object of its
 * kind exists and provides single point of access to it for any other code.
 */
const logger = LogManager.getLogger()
logger.log('Test')
