export default class Logger {
    public log(data: any) {
        const curTime = Date.now()
        console.log(`${curTime}--------------${data}`)
    }
}
