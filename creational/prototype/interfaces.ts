export interface ICloneable {
    clone(): any
}

export interface ICellParams {
    style?: any,
    coordX?: number,
    coordY?: number,
    body?: any
}
