import Cell from "./cell";


/**
 * Prototype is a creational design pattern that allows cloning objects,
 * even complex ones, without coupling to their specific classes.
 */
const baseCell = new Cell({ style: { font: '12' } })

const cell = baseCell.clone().setCoords(0,1)
