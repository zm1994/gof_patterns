import {ICellParams, ICloneable} from "./interfaces";

export default class Cell implements ICloneable {
    private style: any
    private coordX: number | undefined
    private coordY: number | undefined
    private body: any

    constructor(params: ICellParams) {
        this.style = params.style
        this.coordX = params.coordX
        this.coordY = params.coordY
        this.body = params.body
    }

    public get params() {
        return {
            style: this.style,
            coordX: this.coordX,
            coordY: this.coordY,
            body: this.body,
        }
    }

    public setStyle(style: any) {
        this.style = style
        return this
    }

    public setCoords(x: number, y: number) {
        this.coordX = x
        this.coordY = y
        return this
    }

    public setBody(body: any) {
        this.body = body
        return this
    }

    public clone(): Cell {
        return new Cell(this.params)
    }
}
