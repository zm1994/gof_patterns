export interface IFormatter {
    format(): string
}

export interface IFactory {
    createTextFormatter(): IFormatter
    createTableFormatter(): IFormatter
}
