import LibreOfficeFactory from "./libreOfficeFactory";

/**
 * @description
 * Abstract Factory is a creational design pattern, which solves the problem of creating entire product
 * families without specifying their concrete classes.
 */
const factory = new LibreOfficeFactory()

// format table file with libre office formatter
const formatter = factory.createTableFormatter()
formatter.format()


// format text file with libre office formatter
const formatterText = factory.createTextFormatter()
formatterText.format()
