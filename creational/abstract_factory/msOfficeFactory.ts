import {IFactory, IFormatter} from "./interfaces";
import MsTableFormatter from "./msTableFormatter";
import MsTextFormatter from "./msTextFormatter";

export default class MsOfficeFactory implements IFactory{
    createTableFormatter(): IFormatter {
        return new MsTableFormatter();
    }

    createTextFormatter(): IFormatter {
        return new MsTextFormatter();
    }

}
