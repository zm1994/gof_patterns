import {IFactory, IFormatter} from "./interfaces";
import LibreTableFormatter from "./libreTableFormatter";
import LibreTextFormatter from "./libreTextFormatter";

export default class LibreOfficeFactory implements IFactory{
    createTableFormatter(): IFormatter {
        return new LibreTableFormatter();
    }

    createTextFormatter(): IFormatter {
        return new LibreTextFormatter();
    }

}
